import re
import ast
import os
from setuptools import setup

with open('cronic/__init__.py') as f:
    ver = re.search('__version__\s*=\s*(.*)', f.read().decode('utf8')).group(1)
    version = str(ast.literal_eval(ver))


setup(
    name='cronic',
    version=version,
    packages=['cronic',],
    entry_points={
        'console_scripts': [
            'cronic = cronic.main:cli',
        ],
    },
    install_requires=['click',],
)
