#coding=utf8


def merge_maps(*maps):
    rv = {}
    for m in maps:
        for k, v in m.items():
            if v != '$':
                rv.setdefault(k, v)
    
    return rv
