#coding=utf8
from tempfile import NamedTemporaryFile
import subprocess
import os


def read_crontab():
    return subprocess.check_output(['crontab', '-l'])


def write_crontab(content):
    with NamedTemporaryFile('w+t') as f:
        f.write(content)
        f.flush()
        os.system('crontab < {}'.format(f.name))

